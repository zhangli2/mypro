from django.contrib import admin
from .models import Grades, Students,Post

# Register your models here.
@admin.register(Post)
#admin.site.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['pk','title','body','date']
    list_filter = ['title']
    search_fields = ['title']
    list_per_page = 5

@admin.register(Grades)

class GradesAdmin(admin.ModelAdmin):
    list_display = ['pk','gname','gdate','ggirlnum','gboynum','isDelete']
    list_filter = ['gname']
    search_fields = ['gname']
    list_per_page = 5
    fieldsets = [
                    ("num", {"fields": ['ggirlnum', 'gboynum']}),
                    ("base", {"fields": ["gname", "gdate", "isDelete"]}),
    ]
#admin.site.register(Grades,GradesAdmin)

@admin.register(Students)
class StudentAdmin(admin.ModelAdmin):
    def gender(self):
        if self.sgender:
            return "男"
        else:
            return "女"
    gender.short_description = "性别"
    list_display = ['pk','sname','sage',gender,'scontend','isDelete']
    list_per_page = 10
#admin.site.register(Students,StudentAdmin)