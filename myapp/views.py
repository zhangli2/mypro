from django.shortcuts import render



# Create your views here.
#  from django.http import HttpResponse
# def index(request):
#     return HttpResponse('ZL IS A GOOD TEACHER')
# def detail(request,num):
#      return HttpResponse("details-%s" %num)


# from myapp.models import Grades
# def grades(request):
#     # 去Modes取数据
#     gradeslist = Grades.objects.all()
#     #将取出来的数据，传递给模板，交给浏览器渲染
#     return render(request,'myapp/grades.html',{'grades': gradeslist })

from django.views.generic import ListView,DetailView

from myapp.models import Post
from django.core.paginator import Paginator
def index(request):
    postlist = Post.objects.all()
    paginator = Paginator(postlist, 5)
    current_num = int(request.GET.get('page',1))
    postlist = paginator.page(current_num)
    return render(request,'myapp/index.html',{'blogs':postlist})

from django.http import HttpResponse
def detail(request, num):
    detaillist = Post.objects.get(pk=num)
    return render(request,'myapp/detail.html',{'detaillist':detaillist})